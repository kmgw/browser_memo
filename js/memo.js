/* browser_memo */

{
    const save = document.getElementById('save'),
    del = document.getElementById('delete'),
    state = document.getElementById('state');
    stateObj = {
        0 : '新規作成',
        1 : '保存データを読み込みました。',
        2 : 'データを保存しました。',
        3 : '保存するには1文字以上の入力が必要です。'
    };
    // テキストエリア取得
    const content = document.getElementById('content');

    // 削除ボタン押下後に実行するメソッド
    const deleteText =()=> {
        if (window.confirm ('入力した文字を全て削除しますか？')) {
            content.value = '';
            localStorage.removeItem('writeObj');
            state.innerText = stateObj[0];
        }
    }

    // 保存ボタン押下後に実行する保存全体メソッド
    const handlingText =()=> {
        var content = getText();
        
        if (content.length) {
            saveText(content);
        } else {
            state.innerText = stateObj[3];
        }
    }

    // テキスト取得メソッド
    const getText =()=> {
        var content = document.getElementById('content');
        return content.value;
    }

    // テキスト保存メソッド
    const saveText = (content) => {
        localStorage.setItem("writeObj", content);
        state.innerText = stateObj[2];
    }

    // ボタン押下イベント
    save.addEventListener('click', handlingText);
    del.addEventListener('click', deleteText);

    // localStorageからテキストデータを取得
    let writeObj = localStorage.writeObj;

    // 保存データがあれば挿入
    if (writeObj) {
        content.value = writeObj;
        state.innerText = stateObj[1];
    }
}